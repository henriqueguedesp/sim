//FUNÇÃO DE CADASTRO DE USUÁRIO
$(document).ready(function () {
    $('#form').submit(function () {
        var assunto = $('#assunto').val();
        var mensagem = $('#mensagem').val();
        var remetente = $('#remetente').val();
        $("#realizandoEnvio").modal('show');
        $.ajax({//Função AJAX
            url: "mensagemAjuda",
            type: "post",
            data: {assunto: assunto, mensagem: mensagem, remetente : remetente},
            success: function (result) {
                $("#realizandoEnvio").modal('hide');
                if (result == 1) {
                    $("#sucesso").modal('show');
                }
                if (result == 0) {
                    $("#erro").modal('show');
                }
            },
            error: function () {
                $("#realizandoEnvio").modal().hide();

                $("#erro").modal('show');
            }
        });

        return false;
    });
});
