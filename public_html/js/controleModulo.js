//função em que vai monitorar o cadastramento de agendamento
$(document).ready(function () {
    $('#formAtivar').submit(function () {
        var id = $('#idAtivar').val();
        $.ajax({//Função AJAX
            url: "ativarModulo",
            type: "post",
            data: {id: id},
            success: function (result) {
                if (result == 0) {
                    jQuery.noConflict();
                    $("#ativarModulo").hide();
                    $("#sucessoAtivar").modal();
                } else {


                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;
    });
});

$(document).ready(function () {
    $('#formDesativar').submit(function () {
        var id = $('#idDesativar').val();
        var sigla = $('#sigla').val();
        $.ajax({//Função AJAX
            url: "desativarModulo",
            type: "post",
            data: {id: id},
            success: function (result) {
                if (result == 0) {
                    jQuery.noConflict();

                    $("#desativarModulo").hide();
                    $("#sucessoDesativar").modal();
                } else {


                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;
    });
});
$(document).ready(function () {
    $('#formCadastro').submit(function () {
        var nome = $('#nome').val();
        var sigla = $('#sigla').val();
        $.ajax({//Função AJAX
            url: "cadastrarModulo",
            type: "post",
            data: {nome: nome, sigla: sigla},
            success: function (result) {
                if (result == 25) {
                    jQuery.noConflict();

                    $("#cadastrarModulo").hide();
                    $("#sucessoCadastro").modal();
                } else {
                    if (result == 22) {
                        //MOSTRAR PARA O USUÁRIO QUE O NOME JÁ ESTÁ SENDO UTILIZADA 
                        $("#avisoNome").show();
                        $("#avisoNome").removeClass(' alert alert-success');
                        $("#avisoNome").addClass("alert alert-danger animated fadeInUp").html("<center>Nome já utilizado!</center>");
                        document.getElementById('nome').style.border = "2px solid #FF6347";
                        //MOSTRAR PARA O USUÁRIO QUE  A SIGLA JÁ ESTÁ SENDO UTILIZADA 
                        $("#avisoSigla").show();
                        $("#avisoSigla").removeClass(' alert alert-success');
                        $("#avisoSigla").addClass("alert alert-danger animated fadeInUp").html("<center>Sigla já utilizada!</center>");
                        document.getElementById('sigla').style.border = "2px solid #FF6347";
                    }
                    if (result == 23) {
                        $("#avisoNome").show();
                        $("#avisoNome").removeClass(' alert alert-success');
                        $("#avisoNome").addClass("alert alert-danger animated fadeInUp").html("<center>Nome já utilizado!</center>");
                        document.getElementById('nome').style.border = "2px solid #FF6347";
                        //MOSTRAR PARA O USUÁRIO QUE  A SIGLA JÁ ESTÁ SENDO UTILIZADA 
                        $("#avisoSigla").hide();
                        document.getElementById('sigla').style.border = "";
                    }
                    if (result == 24) {
                        $("#avisoNome").hide();
                        document.getElementById('nome').style.border = "";
                        //MOSTRAR PARA O USUÁRIO QUE  A SIGLA JÁ ESTÁ SENDO UTILIZADA 
                        $("#avisoSigla").show();
                        $("#avisoSigla").removeClass(' alert alert-success');
                        $("#avisoSigla").addClass("alert alert-danger animated fadeInUp").html("<center>Sigla já utilizada!</center>");
                        document.getElementById('sigla').style.border = "2px solid #FF6347";
                    }

                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;
    });
});

$(document).ready(function () {
    $('#formEditar').submit(function () {
        var nome = $('#nomeEditar').val();
        var sigla = $('#siglaEditar').val();
        var id = $('#id').val();
        $.ajax({//Função AJAX
            url: "editarModulo",
            type: "post",
            data: {nomeEditar: nome, siglaEditar: sigla, id: id},
            success: function (result) {
                if (result == 25) {
                    jQuery.noConflict();

                    $("#editarModulo").hide();
                    $("#sucessoEditar").modal();
                } else {
                    if (result == 22) {
                        //MOSTRAR PARA O USUÁRIO QUE O NOME JÁ ESTÁ SENDO UTILIZADA 
                        $("#avisoNomeEditar").show();
                        $("#avisoNomeEditar").removeClass(' alert alert-success');
                        $("#avisoNomeEditar").addClass("alert alert-danger animated fadeInUp").html("<center>Nome já utilizado!</center>");
                        document.getElementById('nomeEditar').style.border = "2px solid #FF6347";
                        //MOSTRAR PARA O USUÁRIO QUE  A SIGLA JÁ ESTÁ SENDO UTILIZADA 
                        $("#avisoSiglaEditar").show();
                        $("#avisoSiglaEditar").removeClass(' alert alert-success');
                        $("#avisoSiglaEditar").addClass("alert alert-danger animated fadeInUp").html("<center>Sigla já utilizada!</center>");
                        document.getElementById('siglaEditar').style.border = "2px solid #FF6347";
                    }
                    if (result == 23) {
                        $("#avisoNomeEditar").show();
                        $("#avisoNomeEditar").removeClass(' alert alert-success');
                        $("#avisoNomeEditar").addClass("alert alert-danger animated fadeInUp").html("<center>Nome já utilizado!</center>");
                        document.getElementById('nomeEditar').style.border = "2px solid #FF6347";
                        //MOSTRAR PARA O USUÁRIO QUE  A SIGLA JÁ ESTÁ SENDO UTILIZADA 
                        $("#avisoSiglaEditar").hide();
                        document.getElementById('siglaEditar').style.border = "";
                    }
                    if (result == 24) {
                        $("#avisoNomeEditar").hide();
                        document.getElementById('nomeEditar').style.border = "";
                        //MOSTRAR PARA O USUÁRIO QUE  A SIGLA JÁ ESTÁ SENDO UTILIZADA 
                        $("#avisoSiglaEditar").show();
                        $("#avisoSiglaEditar").removeClass(' alert alert-success');
                        $("#avisoSiglaEditar").addClass("alert alert-danger animated fadeInUp").html("<center>Sigla já utilizada!</center>");
                        document.getElementById('siglaEditar').style.border = "2px solid #FF6347";
                    }

                }
            },
            error: function () {
                alert('Erro 664!');
            }
        });
        return false;
    });
});

function editar(descricao, sigla, id) {
    jQuery.noConflict();
    document.getElementById('nomeEditar').value = descricao;
    document.getElementById('siglaEditar').value = sigla;
    document.getElementById('id').value = id;
    $("#editarModulo").modal();
}

function desativarModulo(id) {
    jQuery.noConflict();
    document.getElementById('idDesativar').value = id;
    $("#desativarModulo").modal();

}

function ativarModulo(id) {
    jQuery.noConflict();
    document.getElementById('idAtivar').value = id;
    $("#ativarModulo").modal();

}