function editar(nome, email, funcao, usuario, senha, id) {
    jQuery.noConflict();
    $("#editarUsuario").modal();
    document.getElementById('nomeE').value = nome;
    document.getElementById('funcaoE').value = funcao;
    document.getElementById('emailE').value = email;
    document.getElementById('usuarioE').value = usuario;
    document.getElementById('senhaE').value = senha;
    document.getElementById('confirmaSenhaE').value = senha;
    document.getElementById('idEditar').value = id;
    //ajax para preenchcher o combobox com as suas permissoes
    $.post('permissoesUsuario', {id: id}, function (data) {
        console.log(data);
        $.each(data, function (index, value) {
            var comboCidades = document.getElementById("@" + value.descricao);
            comboCidades.selectedIndex = value.tipo;
            console.log(value.tipo);
        });
    }, 'json');

}

function confirmacao(id) {
    var resposta = confirm("Deseja desativar o usuário?");
    if (resposta == true) {

        window.location.assign("/sim/public_html/desativarUsuario/" + id);
    }
}

function confirmacaoAtivar(id) {
    var resposta = confirm("Deseja ativar o usuário?");
    if (resposta == true) {

        window.location.assign("/sim/public_html/ativarUsuario/" + id);
    }
}

$(document).ready(function () {
    $('#formEditar').submit(function () {
        document.getElementById('senhaE').style.border = "";
        document.getElementById('confirmaSenhaE').style.border = "";
        var nome = $('#nomeE').val();
        var funcao = $('#funcaoE').val();
        var email = $('#emailE').val();
        var usuario = $('#usuarioE').val();
        var senha = $('#senhaE').val();
        var confirmaSenha = $('#confirmaSenhaE').val();
        var id = $('#idEditar').val();
        if (confirmaSenha != senha) {
            $("#aviso").show();
            $("#aviso").removeClass(' alert alert-success');
            $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Senhas não conferem!</center>");
            document.getElementById('senhaE').style.border = "2px solid #FF6347";
            document.getElementById('confirmaSenhaE').style.border = "2px solid #FF6347";
        } else {
            $.ajax({//Função AJAX
                url: "verificaEditar",
                type: "post",
                data: {nome: nome, funcao: funcao, email: email, usuario: usuario, senha: senha, id: id},
                success: function (result) {
                    document.getElementById('senhaE').style.border = "";
                    document.getElementById('confirmaSenhaE').style.border = "";
                    if (result == 22) {
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Usuário já utilizado!</center>");
                        document.getElementById('usuarioE').style.border = "2px solid #FF6347";
                        $("#aviso").show();
                        $("#aviso").removeClass(' alert alert-success');
                        $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! E-mail já utilizado!</center>");
                        document.getElementById('emailE').style.border = "2px solid #FF6347";
                    } else {
                        if (result == 23) {
                            $("#aviso").show();
                            $("#aviso").removeClass(' alert alert-success');
                            $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Usuário já utilizado!</center>");
                            document.getElementById('usuarioE').style.border = "2px solid #FF6347";
                            $("#aviso").hide();
                            document.getElementById('emailE').style.border = "";
                        } else {
                            if (result == 24) {
                                $("#aviso").show();
                                $("#aviso").removeClass(' alert alert-success');
                                $("#aviso").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Usuário já utilizado!</center>");
                                document.getElementById('emailE').style.border = "2px solid #FF6347";
                                $("#aviso").hide();
                                document.getElementById('usuarioE').style.border = "";
                            } else {
                                if (result) {

                                    var permissoes;
                                    var cont = 0;
                                    $.post('modulosAtivos', {}, function (data) {
                                        $.each(data, function (index, value) {
                                            //alert(value.idModulos);
                                            var ValorA = document.getElementById("@" + value.descricao).value;
                                            if (cont == 0) {
                                                permissoes = ValorA;
                                            } else {
                                                permissoes = permissoes + ":";
                                                permissoes = permissoes + ValorA;
                                            }
                                            cont = 1;
                                        });

                                        $.ajax({//Função AJAX
                                            url: "editarUsuario",
                                            type: "post",
                                            data: {nome: nome, funcao: funcao, email: email, usuario: usuario, senha: senha, id: id, permissoes: permissoes},
                                            success: function (result) {
                                                if (result == 0) {
                                                    $("#editarUsuario").hide();
                                                    jQuery.noConflict();
                                                    $("#sucessoEditar").modal('show');
                                                }
                                            },
                                            error: function () {
                                                alert('Erro 664!');
                                            }
                                        });

                                    }, 'json');
                                }
                            }
                        }
                    }

                }
                ,
                error: function () {
                    alert('Erro 664!');
                }
            }, 'json');
        }
        return false;
    });
});


$(document).ready(function () {
    $('#form').submit(function () {
        var nome = $('#nome').val();
        var funcao = $('#funcao').val();
        var email = $('#email').val();
        var usuario = $('#usuario').val();
        var senha = $('#senha').val();
        var confirmaSenha = $('#confirmaSenha').val();
        if (confirmaSenha != senha) {
            $("#confirmaSenhaC").show();
            $("#confirmaSenhaC").removeClass(' alert alert-success');
            $("#confirmaSenhaC").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Senhas não conferem!</center>");
            document.getElementById('senha').style.border = "2px solid #FF6347";
            document.getElementById('confirmaSenha').style.border = "2px solid #FF6347";
        } else {
            $.ajax({//Função AJAX
                url: "verificaCadastro",
                type: "post",
                data: {nome: nome, funcao: funcao, email: email, usuario: usuario, senha: senha},
                success: function (result) {
                    if (result == 22) {
                        $("#avisoUsuario").show();
                        $("#avisoUsuario").removeClass(' alert alert-success');
                        $("#avisoUsuario").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Usuário já utilizado!</center>");
                        document.getElementById('usuario').style.border = "2px solid #FF6347";
                        $("#avisoEmail").show();
                        $("#avisoEmail").removeClass(' alert alert-success');
                        $("#avisoEmail").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! E-mail já utilizado!</center>");
                        document.getElementById('email').style.border = "2px solid #FF6347";
                    } else {
                        if (result == 23) {
                            $("#avisoUsuario").show();
                            $("#avisoUsuario").removeClass(' alert alert-success');
                            $("#avisoUsuario").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Usuário já utilizado!</center>");
                            document.getElementById('usuario').style.border = "2px solid #FF6347";
                            $("#avisoEmail").hide();
                            document.getElementById('email').style.border = "";
                        } else {
                            if (result == 24) {
                                $("#avisoEmail").show();
                                $("#avisoEmail").removeClass(' alert alert-success');
                                $("#avisoEmail").addClass("alert alert-danger animated fadeInUp").html("<center>Atenção! Usuário já utilizado!</center>");
                                document.getElementById('email').style.border = "2px solid #FF6347";
                                $("#avisoUsuario").hide();
                                document.getElementById('usuario').style.border = "";
                            } else {
                                //alert(result);  

                                if (result) {

                                    var permissoes;
                                    var cont = 0;
                                    $.post('modulosAtivos', {}, function (data) {
                                        $.each(data, function (index, value) {
                                            //alert(value.idModulos);
                                            var ValorA = document.getElementById(value.descricao).value;
                                            if (cont == 0) {
                                                permissoes = ValorA;
                                            } else {
                                                permissoes = permissoes + ":";
                                                permissoes = permissoes + ValorA;
                                            }
                                            cont = 1;
                                        });

                                        $.ajax({//Função AJAX
                                            url: "cadastrarUsuario",
                                            type: "post",
                                            data: {nome: nome, funcao: funcao, email: email, usuario: usuario, senha: senha, permissoes: permissoes},
                                            success: function (result) {
                                                if (result == 0) {
                                                    $("#cadastrarUsuario").hide();
                                                    jQuery.noConflict();
                                                    $("#sucesso").modal('show');
                                                }
                                            },
                                            error: function () {
                                                alert('Erro 664!');
                                            }
                                        });

                                    }, 'json');
                                }

                            }
                        }
                    }

                }
                ,
                error: function () {
                    alert('Erro 664!');
                }
            }, 'json');
        }
        return false;
    });
});