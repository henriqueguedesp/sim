<?php

namespace UBSValorem\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UBSValorem\Util\Sessao;
use UBSValorem\Models\ModeloModulo;

class ControleModulo {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    public function ativarModulo() {
        $usuario = $this->sessao->get('userSIM');
        if ($usuario) {
            $id = $this->request->get('id');
            $modelo = new ModeloModulo();
            $modelo->ativar($id);
            echo 0;
        } else {
            $this->redireciona('/sim/public_html/');
        }
    }

    public function desativarModulo() {
        $usuario = $this->sessao->get('userSIM');
        if ($usuario) {
            $id = $this->request->get('id');
            $modelo = new ModeloModulo();
            $modelo->desativar($id);
            echo 0;
        } else {
            $this->redireciona('/sim/public_html/');
        }
    }

    public function editarModulo() {
        $usuario = $this->sessao->get('userSIM');
        if ($usuario) {
            $modelo = new ModeloModulo();
            $nome = $this->request->get('nomeEditar');
            $sigla = $this->request->get('siglaEditar');
            $id = $this->request->get('id');

            $verificaNome = $modelo->verificaNomeEditar($nome, $id);
            $verificaSigla = $modelo->verificaSiglaEditar($sigla, $id);
            if ($verificaNome != null || $verificaSigla != null) {
                //os dois campos já tem no campo
                if ($verificaNome != null && $verificaSigla != null) {
                    echo 22;
                } else {
                    if ($verificaNome != null) {
                        echo 23;
                    } else {
                        if ($verificaSigla != null) {
                            echo 24;
                        }
                    }
                }
            } else {
                $modulos = $modelo->editar($nome, $sigla, $id);
                echo 25;
            }
        } else {
            $this->redireciona('/sim/public_html/');
        }
    }

    public function cadastrarModulo() {
        $usuario = $this->sessao->get('userSIM');
        if ($usuario) {
            $modelo = new ModeloModulo();
            $nome = $this->request->get('nome');
            $sigla = $this->request->get('sigla');

            $verificaNome = $modelo->verificaNome($nome);
            $verificaSigla = $modelo->verificaSigla($sigla);
            
            if ($verificaNome != null || $verificaSigla != null) {
                //os dois campos já tem no campo
                if ($verificaNome != null && $verificaSigla != null) {
                    echo 22;
                } else {
                    if ($verificaNome != null) {
                        echo 23;
                    } else {
                        if ($verificaSigla != null) {
                            echo 24;
                        }
                    }
                }
            } else {
                $modulos = $modelo->cadastrar($nome, $sigla, $usuario->idUsuario);
                echo 25;
            }
        } else {
            $this->redireciona('/sim/public_html/');
        }
    }

    public function controleModulo() {
        $usuario = $this->sessao->get('userSIM');
        if ($usuario) {
            $modelo = new ModeloModulo();
            $modulos = $modelo->modulos();
            return $this->response->setContent($this->twig->render('modulo/modulo.html.twig', array('user' => $usuario, 'modulos' => $modulos)));
        } else {
            $this->redireciona('/sim/public_html/');
        }
    }

    public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

}
