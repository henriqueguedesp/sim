<?php

namespace UBSValorem\Controllers;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UBSValorem\Util\Sessao;
use UBSValorem\Models\ModeloUsuario;
use UBSValorem\Models\ModeloModulo;
use UBSValorem\Entity\Usuario;

class ControleUsuario {

    private $response;
    private $twig;
    private $request;
    private $sessao;

    public function editarUsuario() {
        $usuario = $this->sessao->get('userSIM');
        if ($usuario && $usuario->tipo == 2) {
            $user = new Usuario();
            $user->setNome($this->request->get('nome'));
            $user->setUsuario($this->request->get('usuario'));
            $user->setEmail($this->request->get('email'));
            $user->setSenha($this->request->get('senha'));
            $user->setFuncao($this->request->get('funcao'));
            $user->setIdUsuario($this->request->get('id'));
            $permissoes = $this->request->get('permissoes');

            $modelo = new ModeloModulo();
            $modulos = $modelo->modulosAtivos();
            $modelo = new ModeloUsuario();
            $per = explode(":", $permissoes);
            $modelo->editar($user, $per, $modulos);
            echo 0;
        } else {
            $this->redireciona('/sim/public_html/login');
        }
    }

    public function verificaEditar() {
        $usuario = $this->sessao->get('userSIM');
        if ($usuario && $usuario->tipo == 2) {
            $user = new Usuario();
            $user->setNome($this->request->get('nome'));
            $user->setUsuario($this->request->get('usuario'));
            $user->setEmail($this->request->get('email'));
            $user->setSenha($this->request->get('senha'));
            $user->setFuncao($this->request->get('funcao'));
            $user->setIdUsuario($this->request->get('id'));
            $modelo = new ModeloUsuario();
            $verificaUsuario = $modelo->verificaUsuarioEditar($user->getUsuario(), $user->getIdUsuario());
            $verificaEmail = $modelo->verificaEmailEditar($user->getEmail(), $user->getIdUsuario());
            if ($verificaUsuario || $verificaEmail) {
                if ($verificaUsuario && $verificaEmail) {
                    echo 22;
                } else {
                    if ($verificaUsuario) {
                        echo 23;
                    } else {
                        if ($verificaEmail) {
                            echo 24;
                        }
                    }
                }
            } else {
                $modelo = new ModeloModulo();
                $modulosAtivos = $modelo->modulosAtivos();
                echo json_encode($modulosAtivos);
            }
        } else {
            $this->redireciona('/sim/public_html/login');
        }
    }

    public function ativarUsuario($id) {
        $usuario = $this->sessao->get('userSIM');
        if ($usuario) {
            $modelo = new ModeloUsuario();
            $modelo->ativar($id);
            echo "<script> alert('Usuário ativado com sucesso!'); "
            . " location.href='/sim/public_html/controleUsuario';</script>";
        } else {
            $this->redireciona('/login');
        }
    }

    public function desativarUsuario($id) {
        $usuario = $this->sessao->get('userSIM');
        if ($usuario) {
            $modelo = new ModeloUsuario();
            $modelo->desativar($id);
            echo "<script> alert('Usuário desativado com sucesso!'); "
            . " location.href='/sim/public_html/controleUsuario';</script>";
        } else {
            $this->redireciona('/sim/public_html/login');
        }
    }

    public function verificaCadastro() {
        $usuario = $this->sessao->get('userSIM');
        if ($usuario && $usuario->tipo == 2) {
            $user = new Usuario();
            $user->setNome($this->request->get('nome'));
            $user->setUsuario($this->request->get('usuario'));
            $user->setEmail($this->request->get('email'));
            $user->setSenha($this->request->get('senha'));
            $user->setFuncao($this->request->get('funcao'));
            $modelo = new ModeloUsuario();
            $verificaUsuario = $modelo->verificaUsuario($user->getUsuario());
            $verificaEmail = $modelo->verificaEmail($user->getEmail());
            if ($verificaUsuario || $verificaEmail) {
                if ($verificaUsuario && $verificaEmail) {
                    echo 22;
                } else {
                    if ($verificaUsuario) {
                        echo 23;
                    } else {
                        if ($verificaEmail) {
                            echo 24;
                        }
                    }
                }
            } else {
                $modelo = new ModeloModulo();
                $modulosAtivos = $modelo->modulosAtivos();
                echo json_encode($modulosAtivos);
            }
        } else {
            $this->redireciona('/sim/public_html/login');
        }
    }

    public function permissoesUsuario() {

        $modelo = new ModeloModulo();
        $permissoes = $modelo->permissoesUsuario($this->request->get('id'));
        echo json_encode($permissoes);
    }

    public function modulosAtivos() {

        $modelo = new ModeloModulo();
        $modulosAtivos = $modelo->modulosAtivos();
        echo json_encode($modulosAtivos);
    }

    public function cadastrarUsuario() {
        $usuario = $this->sessao->get('userSIM');
        if ($usuario && $usuario->tipo == 2) {
            $user = new Usuario();
            $user->setNome($this->request->get('nome'));
            $user->setUsuario($this->request->get('usuario'));
            $user->setEmail($this->request->get('email'));
            $user->setSenha($this->request->get('senha'));
            $user->setFuncao($this->request->get('funcao'));
            $permissoes = $this->request->get('permissoes');

            $modelo = new ModeloModulo();
            $modulos = $modelo->modulosAtivos();
            $modelo = new ModeloUsuario();
            $per = explode(":", $permissoes);
            $modelo->cadastrar($user, $per, $modulos);
            echo 0;
        } else {
            $this->redireciona('/sim/public_html/login');
        }
    }

    public function controleUsuario() {
        $usuario = $this->sessao->get('userSIM');
        if ($usuario && $usuario->tipo == 2) {
            $modelo = new ModeloUsuario();
            $usuarios = $modelo->usuarios($usuario->idUsuario);
            $modelo = new ModeloModulo();
            $modulosAtivos = $modelo->modulosAtivos();
            return $this->response->setContent($this->twig->render('usuario/usuario.html.twig', array('user' => $usuario, 'usuarios' => $usuarios, 'modulos' => $modulosAtivos)));
        } else {
            $this->redireciona('/sim/public_html/login');
        }
    }

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function paginaLogin() {
        $usuario = $this->sessao->get('userSIM');
        if ($usuario) {
            $this->redireciona('/sim/public_html/');
        } else {
            $modelo = new ModeloModulo(); 
            $dados  = $modelo->modulosAtivos();
            //return $this->response->setContent($this->twig->render('Login.html.twig'));
            return $this->response->setContent($this->twig->render('login/login.html.twig', array('dados'=>$dados)));
        }
    }

    public function validaLogin() {
        $modulo =$this->request->get('modulo');
        $usuario = $this->request->get('usuario');
        $senha = $this->request->get('senha');
        $modelo = new ModeloUsuario();
        //$retorno = $modelo->validaLogin($usuario, $senha);
        $validaUsuario = $modelo->validaUsuario($usuario);
        if ($validaUsuario) {
            $usuarioAtivo = $modelo->usuarioAtivo($usuario);
            if ($usuarioAtivo) {
                $valida = $modelo->validaSenha($usuario, $senha);
                if ($valida) {
                    $modelo = new ModeloModulo();
                    $moduloAtivo = $modelo->verificaAtivado($modulo);
                    if ($moduloAtivo) {

                        $modelo = new ModeloUsuario();
                        $acesso = $modelo->validaLogin($usuario, $senha);
                        if ($acesso) {
                            $this->sessao->add("userSIM", $acesso);
                            echo 1;
                        } else {
                            echo 14;
                        }
                    } else {
                        echo 13;
                    }
                } else {
                    echo 12;
                }
            } else {
                echo 11;
            }
        } else {
            echo 10;
        }

        //final
        /*
          if ($retorno) {
          $this->sessao->add("usuario", $retorno);
          echo 1;
          } else {
          echo 0;
          } */
    }

    public function removerUsuario() {
        if ($this->sessao->get("userSIM")) {
            $this->sessao->remove('userSIM');
            $this->sessao->delete('userSIM');
            $this->redireciona("/sim/public_html/");
        } else {
            $this->redireciona("/sim/public_html/login");
        }
    }

    public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

}
