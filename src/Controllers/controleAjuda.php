<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace UBSValorem\Controllers;

/**
 * Description of controleAjuda
 *
 * @author henrique
 */
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\RedirectResponse;
use UBSValorem\Entity\ajuda;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;
use UBSValorem\Util\Sessao;

class controleAjuda {

    private $response;
    private $twig;
    private $request;
    private $sessao;
    private $raiz = '/sim/public_html/';

    function __construct(Response $response, \Twig_Environment $twig, \Symfony\Component\HttpFoundation\Request $request, Sessao $sessao) {
        $this->response = $response;
        $this->twig = $twig;
        $this->request = $request;
        $this->sessao = $sessao;
    }

    public function redireciona($destino) {
        $redirect = new RedirectResponse($destino);
        $redirect->send();
    }

    public function mensagemAjuda() {

        $ajuda = new ajuda();
        $ajuda->setAssunto($this->request->get('assunto'));
        $ajuda->setMensagem($this->request->get('mensagem'));
        $ajuda->setRemetente($this->request->get('remetente'));
            $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
            $mail->IsSMTP(); // Define que a mensagem será SMTP
            $mail->Host = "smtp.ubsvalorem.com.br"; // Seu endereço de host SMTP
            $mail->SMTPAuth = true; // Define que será utilizada a autenticação -  Mantenha o valor "true"
            $mail->Port = 587; // Porta de comunicação SMTP - Mantenha o valor "587"
            $mail->SMTPSecure = false; // Define se é utilizado SSL/TLS - Mantenha o valor "false"
            $mail->SMTPAutoTLS = false; // Define se, por padrão, será utilizado TLS - Mantenha o valor "false"
            $mail->Username = 'atendimento@ubsvalorem.com.br'; // Conta de email existente e ativa em seu domínio
            $mail->Password = 'tlc@2013'; // Senha da sua conta de email
// DADOS DO REMETENTE
            $mail->Sender = "atendimento@ubsvalorem.com.br"; // Conta de email existente e ativa em seu domínio
            $mail->From = "atendimento@ubsvalorem.com.br"; // Sua conta de email que será remetente da mensagem
            $mail->FromName = "Solicitação Ajuda - SIM"; // Nome da conta de email
// DADOS DO DESTINATÁRIO
            $mail->AddAddress('helpdesk@ubsvalorem.com.br', 'Help Desk Valorem'); // Define qual conta de email receberá a mensagem
//$mail->AddAddress('recebe2@dominio.com.br'); // Define qual conta de email receberá a mensagem
            //$mail->AddCC($usuario->email); // Define qual conta de email receberá uma cópia
//$mail->AddBCC('copiaoculta@dominio.info'); // Define qual conta de email receberá uma cópia oculta
// Definição de HTML/codificação
            $mail->IsHTML(true); // Define que o e-mail será enviado como HTML
            $mail->CharSet = 'utf-8'; // Charset da mensagem (opcional)
// DEFINIÇÃO DA MENSAGEM
            $mail->Subject = $ajuda->getAssunto(); // Assunto da mensagem
            $mail->Body .= $ajuda->getMensagem() . '<br><br><br>'; // Texto da mensagem
            $mail->Body .= '<b>Solicitante do Help:</b> '.$ajuda->getRemetente() .'<br>'; // Texto da mensagem

            $enviado = $mail->Send();
// Limpa os destinatários e os anexos
            $mail->ClearAllRecipients();

// Exibe uma mensagem de resultado do envio (sucesso/erro)
            if ($enviado) {
                echo 1;
            } else {
                echo 0;
                //echo "<b>Detalhes do erro:</b> " . $mail->ErrorInfo;
            }
       
    }

}
