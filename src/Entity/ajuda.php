<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace UBSValorem\Entity;

/**
 * Description of ajuda
 *
 * @author henrique
 */
class ajuda {
    //put your code here
    
    private $assunto;
    private $mensagem;
    private $idMensagem;
    private $remetente;
    private $dataEnvio;
    
    function __construct() {
        
    }
    
    function getAssunto() {
        return $this->assunto;
    }

    function getMensagem() {
        return $this->mensagem;
    }

    function getIdMensagem() {
        return $this->idMensagem;
    }

    function getRemetente() {
        return $this->remetente;
    }

    function getDataEnvio() {
        return $this->dataEnvio;
    }

    function setAssunto($assunto) {
        $this->assunto = $assunto;
    }

    function setMensagem($mensagem) {
        $this->mensagem = $mensagem;
    }

    function setIdMensagem($idMensagem) {
        $this->idMensagem = $idMensagem;
    }

    function setRemetente($remetente) {
        $this->remetente = $remetente;
    }

    function setDataEnvio($dataEnvio) {
        $this->dataEnvio = $dataEnvio;
    }


    
}
