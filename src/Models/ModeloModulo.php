<?php

namespace UBSValorem\Models;

use UBSValorem\Util\Conexao;
use PDO;

class ModeloModulo {

    public function verificaAtivado($sigla) {
        try {
            $sql = "select * from modulos where sigla = :sigla and status = 1";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':sigla', $sigla);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function ativar($id) {
        //apontamento para apontamento Genrico
        try {
            $sql = "update  modulos  set status = 1 where idModulos = :id";

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $ex) {
            
        }
    }

    public function desativar($id) {
        //apontamento para apontamento Genrico
        try {
            $sql = "update  modulos  set status = 0 where idModulos = :id";

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $ex) {
            
        }
    }

    public function editar($nome, $sigla, $id) {
        //apontamento para apontamento Genrico
        try {
            $sql = "update  modulos  set descricao = :descricao , sigla = :sigla where idModulos = :id";

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':descricao', $nome);
            $p_sql->bindValue(':sigla', $sigla);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return Conexao::getInstance()->lastInsertId();
        } catch (Exception $ex) {
            
        }
    }

    public function permissoesUsuario($id) {
        try {
            $sql = "select * from permissao as p, modulos as m where :id = p.idUsuario and m.idModulos = p.idModulo AND m.status = 1;";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function verificaNomeEditar($nome, $id) {
        try {
            $sql = "select * from modulos where descricao = :nome and idModulos != :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':nome', $nome);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function verificaSiglaEditar($sigla, $id) {
        try {
            $sql = "select * from modulos where sigla = :sigla and idModulos != :id";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':sigla', $sigla);
            $p_sql->bindValue(':id', $id);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function cadastrar($nome, $sigla, $idUsuario) {
        //apontamento para apontamento Genrico
        try {
            $conexao = Conexao::getInstance();

            $sql = "insert into modulos (descricao, sigla,dataCriacao,status)"
                    . " values"
                    . " (:nome,:sigla,now(),1)";
            $conexao->beginTransaction();

            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':nome', $nome);
            $p_sql->bindValue(':sigla', $sigla);
            $p_sql->execute();
            $id = Conexao::getInstance()->lastInsertId();

            $status = "insert into permissao (idUsuario, idModulo, dataCriacao, tipo)"
                    . " values "
                    . "(:idUsuario, :idModulo, now(),:tipo)";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->bindValue(':idModulo', $id);
            $p_status->bindValue(':tipo', 2);
            $p_status->execute();

            $status = "insert into permissao (idUsuario, idModulo, dataCriacao, tipo) "
                    . " select idUsuario, :idModulo, CURRENT_TIMESTAMP(), 0 FROM usuario where idUsuario != :idUsuario";
            $p_status = $conexao->prepare($status);
            $p_status->bindValue(':idUsuario', $idUsuario);
            $p_status->bindValue(':idModulo', $id);
            $p_status->execute();


            $conexao->commit();
        } catch (Exception $ex) {
            
        }
    }

    public function verificaNome($nome) {
        try {
            $sql = "select * from modulos where descricao = :nome ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':nome', $nome);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function verificaSigla($sigla) {
        try {
            $sql = "select * from modulos where sigla = :sigla ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->bindValue(':sigla', $sigla);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function modulos() {
        try {
            $sql = "select * from modulos where sigla != 'SIM' ";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

    public function modulosAtivos() {
        try {
            $sql = "select * from modulos where status = 1 order by descricao asc";
            $p_sql = Conexao::getInstance()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $ex) {
            
        }
    }

}
