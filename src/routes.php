<?php

namespace UBSValorem\Routes;

use Symfony\Component\Routing\RouteCollection;
use Symfony\Component\Routing\Route;

$rotas = new RouteCollection();

$rotas->add('raiz', new Route('/', array('_controller' => 
    'UBSValorem\Controllers\ControleIndex',
    '_method' => 'dashboard'))); 

##LOGIN
$rotas->add('login', new Route('/login  ', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'paginaLogin'))); 

$rotas->add('validaLogin', new Route('/validaLogin', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'validaLogin'))); 

$rotas->add('removerUsuario', new Route('/removerUsuario', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'removerUsuario'))); 

$rotas->add('controleUsuario', new Route('/controleUsuario', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'controleUsuario'))); 

$rotas->add('cadastrarUsuario', new Route('/cadastrarUsuario', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'cadastrarUsuario'))); 

$rotas->add('editarUsuario', new Route('/editarUsuario/{_param}', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'editarUsuario'))); 

$rotas->add('ativarUsuario', new Route('/ativarUsuario/{_param}', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'ativarUsuario'))); 

$rotas->add('desativarUsuario', new Route('/desativarUsuario/{_param}', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'desativarUsuario'))); 


$rotas->add('modulosAtivos', new Route('/modulosAtivos', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'modulosAtivos'))); 

$rotas->add('permissoesUsuario', new Route('/permissoesUsuario', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'permissoesUsuario'))); 

$rotas->add('erro404', new Route('/erro404', array('_controller' => 
    'UBSValorem\Controllers\ControleIndex',
    '_method' => 'erro404'))); 

##CONTROLE DE USUÁRIO
$rotas->add('controleUsuario', new Route('/controleUsuario', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'controleUsuario'))); 

$rotas->add('cadastrarUsuario', new Route('/cadastrarUsuario', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'cadastrarUsuario'))); 

$rotas->add('editarUsuario', new Route('/editarUsuario', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'editarUsuario'))); 

$rotas->add('ativarUsuario', new Route('/ativarUsuario/{_param}', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'ativarUsuario'))); 

$rotas->add('desativarUsuario', new Route('/desativarUsuario/{_param}', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'desativarUsuario'))); 

$rotas->add('verificaEditar', new Route('/verificaEditar', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'verificaEditar'))); 

$rotas->add('erro404', new Route('/erro404', array('_controller' => 
    'UBSValorem\Controllers\ControleIndex',
    '_method' => 'erro404'))); 

##CONTROLE PERFIL
$rotas->add('controlePerfil', new Route('/controlePerfil', array('_controller' => 
    'UBSValorem\Controllers\ControlePerfil',
    '_method' => 'paginaPerfil'))); 

$rotas->add('atualizarPerfil', new Route('/atualizarPerfil', array('_controller' => 
    'UBSValorem\Controllers\ControlePerfil',
    '_method' => 'atualizarPerfil'))); 

#ROTAS DE CONTROLE DE MÓDULOS
$rotas->add('controleModulo', new Route('/controleModulo', array('_controller' => 
    'UBSValorem\Controllers\ControleModulo',
    '_method' => 'controleModulo'))); 

$rotas->add('cadastrarModulo', new Route('/cadastrarModulo', array('_controller' => 
    'UBSValorem\Controllers\ControleModulo',
    '_method' => 'cadastrarModulo'))); 

$rotas->add('editarModulo', new Route('/editarModulo', array('_controller' => 
    'UBSValorem\Controllers\ControleModulo',
    '_method' => 'editarModulo'))); 

$rotas->add('desativarModulo', new Route('/desativarModulo', array('_controller' => 
    'UBSValorem\Controllers\ControleModulo',
    '_method' => 'desativarModulo'))); 

$rotas->add('ativarModulo', new Route('/ativarModulo', array('_controller' => 
    'UBSValorem\Controllers\ControleModulo',
    '_method' => 'ativarModulo'))); 

$rotas->add('verificaCadastro', new Route('/verificaCadastro', array('_controller' => 
    'UBSValorem\Controllers\ControleUsuario',
    '_method' => 'verificaCadastro'))); 

$rotas->add('mensagemAjuda', new Route('/mensagemAjuda', array('_controller' => 
    'UBSValorem\Controllers\controleAjuda',
    '_method' => 'mensagemAjuda'))); 

return $rotas;
